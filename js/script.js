
// Завдання
// Написати реалізацію кнопки "Показати пароль".Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:

// У файлі index.html лежить розмітка двох полів вводу пароля.
// Після натискання на іконку поруч із конкретним полем - повинні відображатися символи, які ввів користувач, іконка змінює свій зовнішній вигляд.У коментарях під іконкою - інша іконка, саме вона повинна відображатися замість поточної.
// Коли пароля не видно - іконка поля має виглядати як та, що в першому полі(Ввести пароль)
// Коли натиснута іконка, вона має виглядати, як та, що у другому полі(Ввести пароль)
// Натиснувши кнопку Підтвердити, потрібно порівняти введені значення в полях
// Якщо значення збігаються – вивести модальне вікно(можна alert) з текстом – You are welcome;
// Якщо значення не збігаються - вивести під другим полем текст червоного кольору Потрібно ввести однакові значення

// Після натискання на кнопку сторінка не повинна перезавантажуватись
// Можна міняти розмітку, додавати атрибути, теги, id, класи тощо.


const eyes = document.querySelectorAll('.icon-password');

eyes.forEach(eye => {
  eye.addEventListener('click', () => {
    eye.classList.toggle('fa-eye-slash')
    const input = eye.previousElementSibling
    if (input.type === 'password') {
      input.type = 'text'
    } else {
      input.type = 'password'
    }
  })
});

const form = document.querySelector('.password-form');
const error = form.querySelector('.error')
form.addEventListener('submit', (event) => {
  event.preventDefault()
  if (form.password.value === form.confirmPassword.value) {
    error.textContent = '';
    alert('You are welcome')
    form.reset()
    form.password.focus()
  } else {
    error.textContent = 'Потрібно ввести однакові значення';
  }
})